<?php

/**
* AllClients Account ID and API Key.
*/
$account_id   = '2173';
$api_key      = '2FA324A659D567C2B4A292E533F592EC';

/**
 * The API endpoint and timezone. https://tools.dispensaryresource.com/api/2/
 */
$api_endpoint = 'https://tools.dispensaryresource.com/api/2/';
$api_timezone = new DateTimeZone('America/Los_Angeles');

/**
 * Require and instantiate the API wrapper.
 */
require 'class-allclients-api.php';
$api = new AllClientsAPI($api_endpoint, $account_id, $api_key);

/**
 * Get contact flags.
 */
$flag_options = array();
$flags_xml = $api->method('GetFlags');
if ($flags_xml === false) {
	$flag_options[] = $api->getLastError();
} elseif (isset($flags_xml->error)) {
	$flag_options[] = (string) $flags_xml->error;
} else {
	foreach ($flags_xml->flags->flag as $flag) {
		$flag_options[(int) $flag->flagid] = (string) $flag->name;
	}
}

/**
 * Get todo plans.
 */
$todo_plan_options = array();
$todo_plans_xml = $api->method('GetToDoPlans');
if ($todo_plans_xml === false) {
	$todo_plan_options[] = $api->getLastError();
} elseif (isset($todo_plans_xml->error)) {
	$todo_plan_options[] = (string) $todo_plans_xml->error;
} else {
	foreach ($todo_plans_xml->todoplans->todoplan as $plan) {
		$todo_plan_options[(int) $plan->id] = (string) $plan->name;
	}
}

/**
 * Messages and errors for output.
 */
$messages = array();
$errors   = array();
/**
 * Newline character, to support browser or CLI output.
 */
$nl = php_sapi_name() === 'cli' ? "\n" : "<br>";

$template_data = array(
			'id' => 9,
);

/**
 * Create new contact with AddContact method.
 *
 * @var SimpleXMLElement $contact_xml
 */
if (false === ($contact_xml = $api->method('GetEmailTemplate', $template_data))) {
	throw new Exception($api->getLastError());
}

/**
 * Handle API exceptions or get contact ID.
 */
if (isset($contact_xml->error)) {
	throw new Exception('ANDREW API error: ' . $contact_xml->error);
}

foreach ($contact_xml->emailtemplates->emailtemplate as $email) {
		printf("Email Info %d{$nl}", $email->subject);
		
	}
?>