<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">  	
    <title>Greenito</title>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:700,800,400" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <style>
        body {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        #bg {
            z-index: 0;
            position: fixed;
            top: -50%;
            left: -50%;
            width: 200%;
            height: 200%;
        }
        #bg img {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            min-width: 50%;
            min-height: 50%;
        }
        .page-container {
            position: relative;
            z-index: 2;
            width: 90%;
            height: 100%;
            margin: 20px auto 0 auto;
            padding-top: 20px;
        }
        .callout-wrap {
            position: relative;
            text-align: center;
            max-width: 620px;
        }
        .callout-wrap .callout-wrap-inner {
            position: relative;
            width: 100%;
            padding: 20px;
            background-color: #74A272;
            border: 1px solid transparent;
            border-radius: 8px;
            opacity: 0.92;
            filter: alpha(opacity=92);
            -webkit-box-shadow: 8px 8px 10px 0px rgba(0, 0, 0, 0.15);
            box-shadow: 8px 8px 10px 0px rgba(0, 0, 0, 0.15);
        }
        .callout-wrap .callout-wrap-inner > img:first-child {
            position: relative;
            margin-top: -10px;
            margin-bottom: 10px;
            max-width: 100%;
            height: auto;
        }
        .callout-wrap .callout {
            font-family: 'Open Sans', Helvetica, Arial, sans-serif;
            position: relative;
        }
        .callout-wrap .callout h1 {
            font-size: 28px;
            font-weight: 700;
            color: #ffffff;
            margin: 0 0 20px 0;
        }
        .callout-wrap .callout .subtext {
            font-size: 18px;
            color: #ffeeee;
            margin-bottom: 10px;
        }
        .callout-wrap .callout .ctaButton {
            display: block;
            padding: 9px 0 8px 0;
            font-size: 23px;
            font-weight: 700;
            color: #ffffff;
            text-shadow: 1px 1px 1px #000000;
            border: 1px solid transparent;
            border-radius: 5px;
            width: 100%;
            background-color: #0e3c0c;
            margin: 0 auto 10px auto;
        }
        .callout-wrap .callout .footer {
            font-size: 14px;
            color: #ffeeee;
        }
        #formModal.modal {
            font-family: 'Open Sans', Helvetica, Arial, sans-serif;
        }
        #formModal.modal .modal-header {
            padding-bottom: 0;
            border-color: #080808;
        }
        #formModal.modal .modal-header .progress {
            position: relative;
            max-width: 80%;
            margin: 0 auto 5px auto;
            height: 30px;
        }
        #formModal.modal .modal-header .progress .progress-bar {
            opacity: 0.3;
            filter: alpha(opacity=30);
        }
        #formModal.modal .modal-header .progress span {
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            text-align: center;
            font-size: 12px;
            line-height: 30px;
            font-weight: 700;
        }
        @media (max-width: 768px) {
            #formModal.modal .modal-header .progress {
                max-width: 90%;
                margin-bottom: 5px;
            }
        }
        #formModal.modal .modal-header .modal-title {
            display: block;
            max-width: 85%;
            font-size: 14px;
            font-weight: 700;
            text-align: center;
            margin: 0 auto 5px auto;
        }
        @media (max-width: 768px) {
            #formModal.modal .modal-header .modal-title {
                font-size: 12px;
                height: auto;
            }
        }
        @media (max-width: 768px) {
            #formModal.modal .modal-body {
                padding: 10px;
            }
        }
        #formModal.modal .modal-body .book-graphic {
            width: 33%;
            vertical-align: top;
        }
        #formModal.modal .modal-body .book-graphic img {
            padding-top: 20px;
            max-width: 100%;
            height: auto;
        }
        @media (max-width: 768px) {
            #formModal.modal .modal-body .book-graphic {
                display: none;
            }
        }
        #formModal.modal .modal-body .form-cell {
            padding: 0 25px 0 20px;
        }
        #formModal.modal .modal-body .form-cell > h2 {
            font-size: 24px;
            font-weight: 700;
            text-align: center;
            margin: 0 auto 20px 0;
        }
        @media (max-width: 768px) {
            #formModal.modal .modal-body .form-cell > h2 {
                font-size: 22px;
            }
        }
        #formModal.modal .modal-body .form-cell .formTable {
            color: #000000;
            display: block;
            width: 100%;
        }
        #formModal.modal .modal-body .form-cell .formTable tbody,
        #formModal.modal .modal-body .form-cell .formTable tr {
            display: block;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td {
            display: block;
            width: 100%;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .field,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect,
        #formModal.modal .modal-body .form-cell .formTable tr > td .submitButton,
        #formModal.modal .modal-body .form-cell .formTable tr > td .morelabel,
        #formModal.modal .modal-body .form-cell .formTable tr > td .checkLabel {
            font-family: 'Open Sans', 'Helvetica', serif;
            font-weight: 400;
            font-size: 18px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .field,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect,
        #formModal.modal .modal-body .form-cell .formTable tr > td .submitButton,
        #formModal.modal .modal-body .form-cell .formTable tr > td .morelabel {
            margin-bottom: 10px;
            display: block;
            width: 100%;
            height: 45px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .field.fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldComments.fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect.fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .submitButton.fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .morelabel.fieldComments {
            height: 100px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .field,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldComments,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect {
            border-radius: 5px;
            border: 1px solid #bbbbbb;
            text-indent: 10px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-2,
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-3 {
            box-sizing: border-box;
            display: inline-block;
            float: left;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-2 {
            width: 195px;
        }
        @media (max-width: 640px) {
            #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-2 {
                width: 145px;
            }
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-2:first-child {
            margin-right: 10px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-3 {
            width: 32%;
            margin-right: 1%;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .fieldSelect-3:last-child {
            margin-right: 0;
            width: 34%;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .checkLabel {
            color: #222222;
            text-indent: 10px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .morelabel {
            text-align: left;
            color: #222222;
            margin-bottom: 5px;
            height: auto;
            font-size: 21px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .blockLabel {
            font-size: 16px;
            margin-bottom: 2px;
        }
        #formModal.modal .modal-body .form-cell .formTable tr > td .submitButton {
            text-align: center;
            border-radius: 5px;
            border: 1px solid transparent;
            background-color: #3ba3ff;
            color: white;
            font-size: 20px;
            font-weight: 700;
            font-family: 'Open Sans', 'Helvetica', serif;
        }
        #formModal.modal .modal-body .form-cell .formTable tr td.labelCol {
            display: none;
        }
        #formModal.modal .modal-body .form-cell .footer {
            font-size: 12px;
            text-align: center;
            color: #333333;
            margin-bottom: 10px;
        }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script>
    ;(function ( $, window, document, undefined ) {
        "use strict";
    
        // Create the defaults once
        var pluginName = "acFormExtra",
            defaults = {
                display: "table"
            };
    
        // The actual plugin constructor
        function Plugin ( element, options ) {
            this.element = element;
            this.settings = $.extend( {}, defaults, options );
            this._defaults = defaults;
            this._name = pluginName;
            this.init();
        }
    
        $.extend(Plugin.prototype, {
            /**
             * Initialize plugin
             */
            init: function () {
                // Inline form
                if (this.settings.display === 'inline') {
                    this.inlineForm();
                }
                // Find and apply form submit button override
                var $override = $('#form-button-override');
                if ($override.length) {
                    $(this.element).find('input.submitButton')
                        .attr('value', $override.text().trim())
                        .css('background-color', $override.css('background-color'));
                }
            },
    
            /**
             * Convert table based form into inline
             */
            inlineForm: function() {
                var $row, $label, $controls, $checkbox;
                // Iterate form table rows
                $(this.element).find('.formTable tr').each(function() {
                    $row = $(this);
                    $label = $row.find('div.fieldLabel');
                    if ($label.length) {
                        $controls = $(this).find('input.field, select.fieldSelect, textarea.fieldComments');
                        if ($label.hasClass('morelabel')) {
                            $label.prev('br').remove();
                            $label.removeClass('fieldLabel');
                        }
                        if ($controls.length) {
                            if ($controls.filter('input.field').length) {
                                // Text placeholder
                                $controls.filter('input.field').attr('placeholder', $label.text());
                                $label.parent().addClass('labelCol'); // Hides label
                            } else if ($controls.filter('textarea.fieldComments').length) {
                                // Textarea placeholder
                                $controls.filter('textarea.fieldComments').attr('placeholder', $label.text());
                                $label.parent().addClass('labelCol'); // Hides label
                            } else if ($controls.filter('select.fieldSelect').length) {
                                // Add fieldSelect-1, fieldSelect-2, fieldSelect-3 classes depending on how many in row 
                                $controls.filter('select.fieldSelect').addClass('fieldSelect-' + $controls.filter('select.fieldSelect').length);
                                // Show label block if not empty
                                if ($label.text().length) {
                                    // Convert fieldLabel to moreLabel above select(s)
                                    $label.addClass('morelabel blockLabel')
                                        .removeClass('fieldLabel');
                                } else {
                                    $label.parent().addClass('labelCol');
                                }
                            }
                        }
                    } else {
                        $checkbox = $(this).find('input.fieldCheckbox');
                        if ($checkbox.length) {
                            $(this).find('td').addClass('checkboxCol');
                            $label = $(this).find('label.checkLabel');
                            if ($label.length) {
                                $checkbox.addClass('checkbox-custom');
                                $label.addClass('checkbox-custom-label');
                            }
                        }
                    }
                });
            }
        });
    
        $.fn[ pluginName ] = function ( options ) {
            return this.each(function() {
                if ( !$.data( this, "plugin_" + pluginName ) ) {
                    $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
                }
            });
        };
    
    })( jQuery, window, document );
    </script>
    <script>
        $(document).ready(function() {
            $('.form-cell form').acFormExtra({
                display: 'inline'
            });
            function centerModal() {
                $(this).css('display', 'block');
                var $dialog      = $(this).find(".modal-dialog"),
                    offset       = ($(window).height() - $dialog.height()) / 2,
                    bottomMargin = parseInt($dialog.css('marginBottom'), 10);
                if (offset < bottomMargin) {
                    offset = bottomMargin;
                }
                $dialog.css("margin-top", offset);
            }
            $('.modal').on('show.bs.modal', centerModal);
            $(window).on("resize", function () {
                $('.modal:visible').each(centerModal);
            });
        });
    </script>
</head>
<body>
<div id="bg"><img src="https://06bf840d62c3ffc3e33c-36170c82ccd7fcc9942a3e174701ea04.ssl.cf1.rackcdn.com/coffee-background.jpg" /></div>
<div class="page-container">
    <div class="callout-wrap pull- type:text; label:"Main Box Alignment"; default:"left"; tip:"Enter 'left' or 'right'";">
        <div class="callout-wrap-inner" style="background-color: type:color; label:"Main Box Color"; default:"#74A272"; max-length=7; margin-bottom:0;">
	<img src="http://beta.greenito.com/dr_crm/logo.png" style="margin-bottom:0;" />            
		
            <div class="callout">
                <h1 style="color: type:color; label:"Main Headline Color"; default:"#FFFFFF"; max-length=7; ;">Thank you!</h1>
                <div class="subtext">Deals will be coming to your inbox soon.</div>
                <!--<button class="ctaButton" data-toggle="modal" data-target="#formModal"
                        style="background-color:  type:color; label:"Main Button Color"; default:"#0E3C0C"; max-length=7; ">Start getting deals!
                label:"Main Button Text"; type:text; default:"Download The Report"; </button>-->
            </div>
        </div>
    </div>
</div>
</body>
</html>