<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>Greenito</title>
    <title>Pam's Bakery</title>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:700,800,400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" type="text/css">
    <style>
        header,
        .body-tint,
        .footer-tint {
            background-color: #89d784;
        }
        header {
          text-align: center;
        }
        header > img {
          margin: 4px auto 10px auto;
          max-height: 65px;
          width: auto;
        }
        section.image-backdrop {
          position: relative;
          background-image: url("https://06bf840d62c3ffc3e33c-36170c82ccd7fcc9942a3e174701ea04.ssl.cf1.rackcdn.com/kitchen-image-1920.jpg");
          background-position: center;
          background-size: cover;
          min-height: 700px;
        }
        section.image-backdrop .body-tint {
          position: absolute;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          opacity: 0.1;
          filter: alpha(opacity=10);
          z-index: 0;
        }
        section.image-backdrop .row {
          position: relative;
          z-index: 1;
        }
        section.image-backdrop .row.headlines {
          margin: 100px auto 40px auto;
        }
        section.image-backdrop .row.headlines h1,
        section.image-backdrop .row.headlines h2 {
          position: relative;
          text-align: center;
          color: #FFFFFF;
          font-family: 'Open Sans', 'Helvetica', serif;
        }
        section.image-backdrop .row.headlines h1 {
          font-size: 48px;
          font-weight: 800;
          margin-bottom: 0;
        }
        section.image-backdrop .row.headlines h2 {
          margin: 10px auto 0 auto;
          font-size: 32px;
          font-weight: 700;
        }
        section.image-backdrop .row.headlines h2 span {
          display: inline-block;
          position: relative;
          min-width: 500px;
        }
        section.image-backdrop .row.headlines h2 span .arrow-pointer {
          position: absolute;
          right: -30px;
          top: 20px;
        }
        @media (max-width: 640px) {
          section.image-backdrop .row.headlines h2 span {
            min-width: inherit;
          }
          section.image-backdrop .row.headlines h2 span .arrow-pointer {
            display: none;
          }
        }
        @media (max-width: 992px) {
          section.image-backdrop .row.headlines h1 {
            font-size: 42px;
          }
          section.image-backdrop .row.headlines h2 {
            font-size: 25px;
          }
        }
        @media (max-width: 768px) {
          section.image-backdrop .row.headlines h1 {
            font-size: 38px;
          }
          section.image-backdrop .row.headlines h2 {
            font-size: 22px;
          }
        }
        @media (max-width: 640px) {
          section.image-backdrop .row.headlines {
            width: 350px;
            margin-bottom: 25px;
          }
          section.image-backdrop .row.headlines h1 {
            font-size: 30px;
            font-weight: 700;
            margin-bottom: 0;
          }
          section.image-backdrop .row.headlines h2 {
            margin-top: 10px;
            font-size: 20px;
          }
        }
        section.image-backdrop .row.form {
          text-align: center;
          margin-bottom: 100px;
        }
        section.image-backdrop .row.form table.formTable {
          margin: 0 auto 0 auto;
        }
        section.image-backdrop .row.form table.formTable tr > td {
          display: block;
          width: 100%;
        }
        section.image-backdrop .row.form table.formTable tr > td .field,
        section.image-backdrop .row.form table.formTable tr > td .fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect,
        section.image-backdrop .row.form table.formTable tr > td .submitButton,
        section.image-backdrop .row.form table.formTable tr > td .morelabel,
        section.image-backdrop .row.form table.formTable tr > td .checkLabel {
          font-family: 'Open Sans', 'Helvetica', serif;
          font-weight: 400;
          font-size: 18px;
        }
        section.image-backdrop .row.form table.formTable tr > td .field,
        section.image-backdrop .row.form table.formTable tr > td .fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect,
        section.image-backdrop .row.form table.formTable tr > td .submitButton,
        section.image-backdrop .row.form table.formTable tr > td .morelabel {
          margin-bottom: 10px;
          display: block;
          width: 400px;
          height: 50px;
        }
        section.image-backdrop .row.form table.formTable tr > td .field.fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .fieldComments.fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect.fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .submitButton.fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .morelabel.fieldComments {
          height: 100px;
        }
        @media (max-width: 640px) {
          section.image-backdrop .row.form table.formTable tr > td .field,
          section.image-backdrop .row.form table.formTable tr > td .fieldComments,
          section.image-backdrop .row.form table.formTable tr > td .fieldSelect,
          section.image-backdrop .row.form table.formTable tr > td .submitButton,
          section.image-backdrop .row.form table.formTable tr > td .morelabel {
            width: 300px;
          }
        }
        section.image-backdrop .row.form table.formTable tr > td .field,
        section.image-backdrop .row.form table.formTable tr > td .fieldComments,
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect {
          border-radius: 5px;
          border: 1px solid #bbbbbb;
          text-indent: 10px;
        }
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect-2,
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect-3 {
          box-sizing: border-box;
          display: inline-block;
          float: left;
        }
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect-2 {
          width: 195px;
        }
        @media (max-width: 640px) {
          section.image-backdrop .row.form table.formTable tr > td .fieldSelect-2 {
            width: 145px;
          }
        }
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect-2:first-child {
          margin-right: 10px;
        }
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect-3 {
          width: 127px;
          margin-right: 10px;
        }
        @media (max-width: 640px) {
          section.image-backdrop .row.form table.formTable tr > td .fieldSelect-3 {
            width: 93px;
          }
        }
        section.image-backdrop .row.form table.formTable tr > td .fieldSelect-3:last-child {
          margin-right: 10px;
        }
        section.image-backdrop .row.form table.formTable tr > td .checkLabel {
          color: #f5f5f5;
          text-indent: 10px;
        }
        section.image-backdrop .row.form table.formTable tr > td .morelabel {
          text-align: left;
          color: #f5f5f5;
          margin-bottom: 5px;
          height: auto;
          font-size: 21px;
        }
        section.image-backdrop .row.form table.formTable tr > td .blockLabel {
          font-size: 16px;
          margin-bottom: 2px;
        }
        section.image-backdrop .row.form table.formTable tr > td .submitButton {
          text-align: center;
          border-radius: 5px;
          border: 1px solid transparent;
          background-color: #56c14e;
          color: white;
          font-size: 20px;
          font-weight: 700;
          font-family: 'Open Sans', 'Helvetica', serif;
        }
        section.image-backdrop .row.form table.formTable tr td.labelCol {
          display: none;
        }
        footer {
          position: relative;
          background-color: white;
        }
        footer .footer-tint {
          position: absolute;
          left: 0;
          top: 0;
          width: 100%;
          height: 100%;
          opacity: 0.25;
          filter: alpha(opacity=25);
          z-index: 0;
        }
        footer .row {
          position: relative;
          z-index: 1;
          height: 55px;
          line-height: 55px;
          text-align: center;
        }
        @media (max-width: 640px) {
          header > img {
            margin: 2px auto 6px auto;
            max-height: 55px;
          }
        }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        /* HTML5 Placeholder jQuery Plugin - v2.1.1
         * Copyright (c)2015 Mathias Bynens
         * 2015-03-11
         */
        !function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof module&&module.exports?require("jquery"):jQuery)}(function(a){function b(b){var c={},d=/^jQuery\d+$/;return a.each(b.attributes,function(a,b){b.specified&&!d.test(b.name)&&(c[b.name]=b.value)}),c}function c(b,c){var d=this,f=a(d);if(d.value==f.attr("placeholder")&&f.hasClass(m.customClass))if(f.data("placeholder-password")){if(f=f.hide().nextAll('input[type="password"]:first').show().attr("id",f.removeAttr("id").data("placeholder-id")),b===!0)return f[0].value=c;f.focus()}else d.value="",f.removeClass(m.customClass),d==e()&&d.select()}function d(){var d,e=this,f=a(e),g=this.id;if(""===e.value){if("password"===e.type){if(!f.data("placeholder-textinput")){try{d=f.clone().attr({type:"text"})}catch(h){d=a("<input>").attr(a.extend(b(this),{type:"text"}))}d.removeAttr("name").data({"placeholder-password":f,"placeholder-id":g}).bind("focus.placeholder",c),f.data({"placeholder-textinput":d,"placeholder-id":g}).before(d)}f=f.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id",g).show()}f.addClass(m.customClass),f[0].value=f.attr("placeholder")}else f.removeClass(m.customClass)}function e(){try{return document.activeElement}catch(a){}}var f,g,h="[object OperaMini]"==Object.prototype.toString.call(window.operamini),i="placeholder"in document.createElement("input")&&!h,j="placeholder"in document.createElement("textarea")&&!h,k=a.valHooks,l=a.propHooks;if(i&&j)g=a.fn.placeholder=function(){return this},g.input=g.textarea=!0;else{var m={};g=a.fn.placeholder=function(b){var e={customClass:"placeholder"};m=a.extend({},e,b);var f=this;return f.filter((i?"textarea":":input")+"[placeholder]").not("."+m.customClass).bind({"focus.placeholder":c,"blur.placeholder":d}).data("placeholder-enabled",!0).trigger("blur.placeholder"),f},g.input=i,g.textarea=j,f={get:function(b){var c=a(b),d=c.data("placeholder-password");return d?d[0].value:c.data("placeholder-enabled")&&c.hasClass(m.customClass)?"":b.value},set:function(b,f){var g=a(b),h=g.data("placeholder-password");return h?h[0].value=f:g.data("placeholder-enabled")?(""===f?(b.value=f,b!=e()&&d.call(b)):g.hasClass(m.customClass)?c.call(b,!0,f)||(b.value=f):b.value=f,g):b.value=f}},i||(k.input=f,l.value=f),j||(k.textarea=f,l.value=f),a(function(){a(document).delegate("form","submit.placeholder",function(){var b=a("."+m.customClass,this).each(c);setTimeout(function(){b.each(d)},10)})}),a(window).bind("beforeunload.placeholder",function(){a("."+m.customClass).each(function(){this.value=""})})}});
    </script>
    <script>
        $(document).ready(function() {
            $('table.formTable tr').each(function() {
                var $label = $(this).find('div.fieldLabel');
                if ($label.length) {
                    var $controls = $(this).find('input.field, select.fieldSelect, textarea.fieldComments');
                    if ($label.hasClass('morelabel')) {
                        $label.prev('br').remove();
                        $label.removeClass('fieldLabel');
                    }
                    if ($controls.length) {
                        if ($controls.filter('input.field').length) {
                            // Text placeholder
                            $controls.filter('input.field').attr('placeholder', $label.text());
                            $label.parent().addClass('labelCol'); // Hides label
                        } else if ($controls.filter('textarea.fieldComments').length) {
                            // Textarea placeholder
                            $controls.filter('textarea.fieldComments').attr('placeholder', $label.text());
                            $label.parent().addClass('labelCol'); // Hides label
                        } else if ($controls.filter('select.fieldSelect').length) {
                            // Add fieldSelect-1, fieldSelect-2, fieldSelect-3 classes depending on how many in row 
                            $controls.filter('select.fieldSelect').addClass('fieldSelect-' + $controls.filter('select.fieldSelect').length);
                            // Show label block if not empty
                            if ($label.text().length) {
                                // Convert fieldLabel to moreLabel above select(s)
                                $label.addClass('morelabel blockLabel')
                                      .removeClass('fieldLabel');
                            } else {
                                $label.parent().addClass('labelCol'); // Hides empty label
                            }
                        }
                    }
                } else {
                    if ($(this).find('input.fieldCheckbox').length) {
                        $(this).find('td').css('text-align', 'left');
                    }
                }
            });
            $('#submitOverride').each(function() {
                $('input.submitButton')
                    .attr('value', $(this).text().trim())
                    .css('background-color', $(this).css('background-color'));
            });
            $('input, textarea').placeholder();
        });
    </script>
</head>
<body>
    <header class="container-fluid">
        <img src="http://beta.greenito.com/dr_crm/logo.png" />
    </header>
    
    <section class="container-fluid image-backdrop" style="background-image: url('https://06bf840d62c3ffc3e33c-36170c82ccd7fcc9942a3e174701ea04.ssl.cf1.rackcdn.com/kitchen-image-1920.jpg')">
        <div class="body-tint"></div>
        <div class="row headlines">
            <h1>You're all set!</h1>
            <h2><span>You will start receiving recipes soon.</span></h2>
        </div>
    </section>
    <footer class="container-fluid">
        <div class="footer-tint"></div>
        <div class="row">Greenito &trade;</div>
    </footer>
</body>
</html>